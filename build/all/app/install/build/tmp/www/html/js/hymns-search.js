
// hymns-search.js
// Search module for the hymnal.
///////////////////////////////////////////////////////////////////////////

// Auto-start search.
body.onload=init_search

// Error-checking.
if(!HymnsText)alert("Missing hymns text data!")

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

// Check the GET parameters to see whether we need to search or not.
function init_search()
{
  var params=cgi_get_params()
  if(params["s"])document.f.s.value=trim(params["s"])
  if(params["submit"]=="Submit")start_search()
}

// Returns all the GET parameters as an associative array (hash table).
function cgi_get_params()
{
  var q=location.search
  if(!q)return
  q=q.substr(1) // rid '?'
  var i,a=[],b=q.split('&')
  for(i=0;i<b.length;i+=2)
  {
    var key=cgi_unescape(b[i])
    var value=cgi_unescape(b[i+1])
    a[key]=value
  }
  return a
}

// Returns a proper string free from CGI encodings.
// Assuming that the Javascript unescape() function works differently.
function cgi_unescape(s)
{
  s=s.replace('+',' ')
  var a
  while(a=/\%(\d\d)/.exec(s))
  {
    var ch=String.fromCharCode(a[1])
    s=s.replace(a[0],ch)
  }
  return s
}

// Regular string trim.
function trim(s)
{
  return s?s.replace(/^\s+/,"").replace(/\s+$/,""):""
}

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

// Start searching.
function start_search()
{
  var s=trim(document.f.s.value)
  if(!s)return search_results("Nothing to search for")

  search_results("Please wait while searching ... ...")
  setTimeout(1,"search_for('"+qesc(s)+"')")
}

// Single-quoted string escape.
function qesc(s)
{
  return s.replace(/\\/g,"\\\\").replace(/\'/g,"\\'")
}

// The search process.
function search_for(s)
{
  // Find all.
  var i,max=HymnsText.length,results=[]
  for(i=0;i<max;i++)
    if(HymnsText[i].indexOf(s)!=-1)results.push(HymnsText[i])
  
  // Display all.
  
}

// Display the search results.
function search_results(s)
{
  document.getElementById("results").innerHTML=s
}




