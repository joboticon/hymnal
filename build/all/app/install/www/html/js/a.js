
// Write the `Go to Hymn' input box.
function go_hymn_box(main)
{
  document.write
  ('<form name=f style=display:inline'
  +' onsubmit="go_'+(main?'num':'hymn')+'(document.f.num.value);return false;">'
  +'Go to Hymn (1-1352)&nbsp; <input type=text size=6 name=num>'
  +' <input type=submit value=Submit></form>')
}

// Same function, but printed from the main page.
function go_num_box()
{
  go_hymn_box(1)
}

// Go to a hymn number.
function go_num(num,pre_dir)
{
  try{num=parseInt(num)}catch(e){alert("Invalid number: "+num);return}
  if(num<1||num>1352){return alert("Number out of range (Valid: 1-1352)")}
  window.location.href=(pre_dir?pre_dir:"")+"hymns/"+num+".html"
}

// Same function, but linked from the hymn pages.
function go_hymn(num)
{
  go_num(num,"../")
}

// Embed the music player into the webpage.
function embed_music(filename)
{
	document.writeln("<audio controls>");
	document.writeln("<source src=\"" + filename + "\" type=\"audio/mpeg\">\n");
	document.writeln("<embed height=\"20\" width=\"100\" src=\"" + filename + "\">");
	document.writeln("</audio>");
}

