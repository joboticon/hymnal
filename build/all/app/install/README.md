#Hynmal

Hymnal for Ubuntu Touch 

Based on the LSM English Hymnal from:
http://www.churchinmontereypark.org/Docs/Hymn/EnglishHymnal/html/index.html

## How to build

Set up [clickable](https://github.com/bhdouglass/clickable) and run `clickable` inside the working directory.

# Known Issues / Help Wanted:

* External Links don't redirect
* No copy and paste (qtwebengine)

